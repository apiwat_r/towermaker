using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TimeManager : MonoBehaviour
{
    [SerializeField] private ResourceCalculator resourceCalculator;
    [SerializeField] private EventManager eventManager;
    [SerializeField] private TextMeshProUGUI DayText;
    [SerializeField] private TextMeshProUGUI RecruitRefreshText;
    [SerializeField] private TextMeshProUGUI EventRefreshText;
    [SerializeField] private GameObject LoseScreen;
    [SerializeField] private ForeCaster foreCaster;
    [SerializeField] private List<Image> timeButton = new List<Image>();
    private Tutorial tutorial;

    public float DayCount { get; private set; } = 1;
    private float DayTimer = 0;
    private float EventTimer = 15;
    private float PersonnelTimer = 15;
    private float ForecasterTimer = 10;

    public int TimeScale { get; private set; } = 1;
    public bool TimeStop { get; private set; } = false;

    private bool timeCheat = false;

    private bool losing = false;

	private void Start()
	{
		tutorial = tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();
    }

	public Image DayTime;
    void Update()
    {
        RecruitRefreshText.text =PersonnelTimer.ToString("0");
        EventRefreshText.text = EventTimer.ToString("0");

        TimeCal();
        EventTimeCal();
        PersonnelTimeCal();
        ForecastTimeCal();

		if (Input.GetKeyDown(KeyCode.L))
		{
            superFastforward();
		}
    }

    //Calculate the total time
    private void TimeCal()
	{
        DayTimer += Time.deltaTime * TimeScale;
        DayTime.fillAmount = (DayTimer / 60);

		if (DayTime.fillAmount == 1)
		{
            DayTimer = 0;
            DayTime.fillAmount = 0;
            DayCount += 1;
            DayText.text = "Day " + DayCount.ToString("0") + "/ 20";
		}

        if(DayCount >= 21 && losing == false)
		{
            AudioManager.instance.Play("Losing");
            losing = true;
            LoseScreen.SetActive(true);
            TimeScale = 0;
		}
    }

    //Calculate when to trigger event
    private void EventTimeCal ()
	{
        EventTimer -= Time.deltaTime * TimeScale;

        if (EventTimer <= 0)
        {
            EventTimer = 20;

            eventManager.addEvent();
        }
    }

    private void PersonnelTimeCal()
	{
        PersonnelTimer -= Time.deltaTime  * TimeScale;

        if (PersonnelTimer <= 0)
        {   
            PersonnelTimer = 15;

            resourceCalculator.addPersonnel();
        }
    }

    private void ForecastTimeCal()
	{
       ForecasterTimer -= Time.deltaTime * TimeScale;

        if(ForecasterTimer <= 0)
		{
            ForecasterTimer = 10;

            foreCaster.ForecastShift();  
        }
	}

    private void superFastforward()
    {
        timeCheat = !timeCheat;

        if (timeCheat)
        {
            TimeScale = 100;
        }
		else
		{
            TimeScale = 1;
		}


        checkTime();
	}

    public void timeScale(int _timeScale)
	{
        if(tutorial != null && tutorial.tutorialIndex == 9)
		{
            tutorial.DisableTimeButton();
		}

        TimeScale = _timeScale;

        timeCheat = false;

        checkTime();
	}

    public void checkTime()
    {
        for (var i = 0; i < timeButton.Count; i++)
        {
            timeButton[i].color = Color.white;
        }

        switch (TimeScale)
        {
            case 0:
                timeButton[0].color = new Color(0.4716f, 0.4716f, 0.4716f);
                break;
            case 1:
                timeButton[1].color = new Color(0.4716f, 0.4716f, 0.4716f);
                break;
            case 2:
                timeButton[2].color = new Color(0.4716f, 0.4716f, 0.4716f);
                break;
            case 4:
                timeButton[3].color = new Color(0.4716f, 0.4716f, 0.4716f);
                break;
            case 100:
                for(var i = 0; i < timeButton.Count; i++)
				{
                    timeButton[i].color = Color.yellow;
				}
                break;

        }
    }

}




