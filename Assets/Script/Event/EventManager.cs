using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class EventManager : MonoBehaviour
{
    [SerializeField] private ResourceCalculator resourceCalculator;
    [SerializeField] private TimeManager timeManager;
    [SerializeField] private UpgradePanel upgradePanel;
    [SerializeField] private GameObject EventlUI;
    [SerializeField] private GameObject EventActiveUI;
    [SerializeField] private GameObject PersonnelChooseTab;
    [SerializeField] private GameObject PersonnelChooseUI;
    [SerializeField] private GameObject AvaiablePersonnelSorter;
    [SerializeField] private GameObject PersonnelActiveSorter;
    [SerializeField] private GameObject ChoosePersonnelTab;
    [SerializeField] private Transform EventAvailableSorter;
    [SerializeField] private Transform EventActiveSorter;
    [SerializeField] private Button embarkButton;
    [SerializeField] private GameObject tutorialQuest;

    [Header ("Quest Tab")]
    [SerializeField] private TextMeshProUGUI QuestName;
    [SerializeField] private TextMeshProUGUI RecAttack;
    [SerializeField] private TextMeshProUGUI playerAttackText;
    [SerializeField] private TextMeshProUGUI succesChance;
    [SerializeField] private TextMeshProUGUI QuestReward;
    [SerializeField] private GameObject QuestAttributeSorter;
    [SerializeField] private GameObject PlayerAttributeSorter;

    [SerializeField] private List<EventTemplate> EventPool = new List<EventTemplate>();
    [SerializeField] private List<GameObject> Attribute = new List<GameObject>();

    [SerializeField] private Button RefreshQuestButton;

    private Tutorial tutorial;
    private bool tutorialCheck = true;
    private List<EventTemplate> EventActive = new List<EventTemplate>();
    private GameObject EventMemorizer;
	private int eventIndex = 0;
    private int playerAttack = 0;
    private bool embarkable = false;

	private void Start()
	{
        tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();
        addEvent();
	}

	private void Update()
	{
        checkRefresh();

		if(AvaiablePersonnelSorter.transform.childCount == 0 || !checkPersonnel())
		{
            embarkButton.interactable = false;
		}
		else if(checkPersonnel())
		{
            embarkButton.interactable = true;
        }
        calculateAttack();
    }

    // Add quest to questSorter + calculating the random stat
    public void addEvent() 
	{
        for (var i = 0; i <EventAvailableSorter.transform.childCount; i++)
        {
            Destroy(EventAvailableSorter.transform.GetChild(i).gameObject);
        }

        for (var i = 0; i < 5; i++)
        {
            eventIndex = Random.Range(0, EventPool.Count);

            int tier = Random.Range(1,4);

            float tierMulti = 0;

			switch (tier)
			{
                case 0:
                    tierMulti = 1f;
                    break;
                case 1:
                    tierMulti = 1f;
                    break;
                case 2:
                    tierMulti = 1.5f;
                    break;
                case 3:
                    tierMulti = 1.5f;
                    break;
            }

            float Time = Mathf.Ceil(EventPool[eventIndex].timeConsume * Random.Range(0.7f, 1.5f));

            EventlUI.GetComponent<EventStat>().NameText.text = EventPool[eventIndex].name;
            EventlUI.GetComponent<EventStat>().RequireAttribute = tier;
            EventlUI.GetComponent<EventStat>().MoneyText.text = Mathf.Ceil(EventPool[eventIndex].reward * Random.Range(0.7f,1.5f) * tierMulti * DayMultiCal() * TowerMultiCal()).ToString();
            EventlUI.GetComponent<EventStat>().AttackText.text = Mathf.Ceil(EventPool[eventIndex].requireAttack * Random.Range(0.7f, 1.5f) * tierMulti * DayMultiCal() * TowerMultiCal()).ToString();
            EventlUI.GetComponent<EventStat>().TimeText.text = "Time: " +Time.ToString() + " second";
            EventlUI.GetComponent<EventStat>().image.sprite = EventPool[eventIndex].artwork;
            EventlUI.GetComponent<EventStat>().TimeConsume = Time;

            if (tutorial != null && tutorialCheck == true)
			{
                Instantiate(tutorialQuest, EventAvailableSorter);

                tutorialCheck = false;
            }
			else
			{
                Instantiate(EventlUI, EventAvailableSorter);
            }
        }

        if (tutorial != null  && !tutorial.tutorialFinish)
        {
            for (var i = 1; i < EventAvailableSorter.childCount; i++)
            {
                EventAvailableSorter.GetChild(i).GetComponent<Button>().interactable = false;
            }
        }
    }

    // Trigger when the player choose quest
    public void activateEvent()
	{
        AudioManager.instance.Play("Button");

        PersonnelChooseTab.SetActive(true);
        EventStat thisEvent = EventSystem.current.currentSelectedGameObject.GetComponent<EventStat>();
        QuestName.text =  thisEvent.NameText.text;
        RecAttack.text = "Rec ATK: " + thisEvent.AttackText.text;
        QuestReward.text = "Gold: " +thisEvent.MoneyText.text;

        EventMemorizer = EventSystem.current.currentSelectedGameObject;
        timeManager.timeScale(0);
      
        for (var i = 0; i < QuestAttributeSorter.transform.childCount; i++)
        {
            Destroy(QuestAttributeSorter.transform.GetChild(i).gameObject);
        }

        for (var i = 0; i < thisEvent.strReq; i++)
		{
            Instantiate(Attribute[0],QuestAttributeSorter.transform);
        }
        for (var i = 0; i < thisEvent.agiReq; i++)
        {
            Instantiate(Attribute[1], QuestAttributeSorter.transform);
        }
        for (var i = 0; i < thisEvent.intReq; i++)
        {
            Instantiate(Attribute[2], QuestAttributeSorter.transform);
        }

        for (var i = 0; i < AvaiablePersonnelSorter.transform.childCount; i++)
		{
             Destroy(AvaiablePersonnelSorter.transform.GetChild(i).gameObject);
		}

        for (var i = 0; i < PersonnelActiveSorter.transform.childCount; i++)
		{
            if (PersonnelActiveSorter.transform.GetChild(i).GetComponent<PersonnelStat>().onQuest == false)
			{
                PersonnelStat thisPersonnel = PersonnelActiveSorter.transform.GetChild(i).GetComponent<PersonnelStat>();

                PersonnelChooseUI.GetComponent<PersonnelStat>().PersonnelMemorizer = PersonnelActiveSorter.transform.GetChild(i).gameObject;

                PersonnelChooseUI.GetComponent<PersonnelStat>().AttackText.text = thisPersonnel.AttackText.text;
                PersonnelChooseUI.GetComponent<PersonnelStat>().NameText.text = thisPersonnel.NameText.text;
                PersonnelChooseUI.GetComponent<PersonnelStat>().attributes = thisPersonnel.attributes;
                PersonnelChooseUI.GetComponent<PersonnelStat>().image.sprite = thisPersonnel.image.sprite;
                    
                Instantiate(PersonnelChooseUI, AvaiablePersonnelSorter.transform);
            }
		}

        if (tutorial != null)
		{
            if (tutorial.tutorialIndex == 6)
			{
                tutorial.continueTutorial();
            }
           
		}
    }

    public bool checkPersonnel()
	{
        embarkable = false;

        for(var i = 0; i <AvaiablePersonnelSorter.transform.childCount; i++)
		{
            if(AvaiablePersonnelSorter.transform.GetChild(i).GetComponent<PersonnelStat>().Mark == true)
			{
                embarkable = true;
			}
		}

        return embarkable;
	}
    //Confirm quest embark
    public void embark()
	{
        EventActiveUI.GetComponent<EventStat>().NameText.text = EventMemorizer.GetComponent<EventStat>().NameText.text;
        EventActiveUI.GetComponent<EventStat>().TimeConsume = EventMemorizer.GetComponent<EventStat>().TimeConsume;
        EventActiveUI.GetComponent<EventStat>().MoneyStat = int.Parse(EventMemorizer.GetComponent<EventStat>().MoneyText.text);
        EventActiveUI.GetComponent<EventStat>().Chance = EventMemorizer.GetComponent<EventStat>().Chance;
        EventActiveUI.GetComponent<EventStat>().image.sprite = EventMemorizer.GetComponent<EventStat>().image.sprite;
        Instantiate(EventActiveUI, EventActiveSorter);

        for (var i = 0; i < AvaiablePersonnelSorter.transform.childCount; i++)
		{
            GameObject PersonnelLoad = AvaiablePersonnelSorter.transform.GetChild(i).GetComponent<PersonnelStat>().PersonnelMemorizer;
            GameObject thisEvent = EventActiveSorter.GetChild(EventActiveSorter.childCount -1).gameObject;

            if (AvaiablePersonnelSorter.transform.GetChild(i).GetComponent<PersonnelStat>().Mark== true)
			{       
                PersonnelLoad.GetComponent<PersonnelStat>().onQuest = true;
                thisEvent.GetComponent<EventTimer>().AddPersonnel(PersonnelLoad);
                
            }
		}

        ChoosePersonnelTab.SetActive(false);

        foreach (Transform child in AvaiablePersonnelSorter.transform) {
            Destroy(child.transform.gameObject);
        }

        AudioManager.instance.Play("Button");

        Destroy(EventMemorizer);

        if(tutorial != null)
		{
            if (tutorial.tutorialIndex == 8)
			{
                tutorial.continueTutorial();
            }
            
		}

        AudioManager.instance.Play("Button");
    }

    //Update function, Calculate the total attack form choose personel
    public  void calculateAttack()
	{
        int conditionMet = 0;
        int strHave = 0;
        int agiHave = 0;
        int intHave = 0;

        playerAttack = 0;

        for (var i = 0; i < AvaiablePersonnelSorter.transform.childCount; i++)
        {
            PersonnelStat ThisPersonnel = AvaiablePersonnelSorter.transform.GetChild(i).GetComponent<PersonnelStat>();

            if (ThisPersonnel.Mark == true)
            {
                playerAttack += int.Parse(ThisPersonnel.AttackText.text);

                if (ThisPersonnel.attributes == PersonnelStat.Attribute.intelligent)
                {
                    intHave++;
                }
                if (ThisPersonnel.attributes == PersonnelStat.Attribute.strength)
                {
                    strHave++;
                }
                if (ThisPersonnel.attributes == PersonnelStat.Attribute.speed)
                {
                    agiHave++;
                }
            }           
        }

        playerAttackText.text = "Party ATK: " + playerAttack.ToString();

        if(EventMemorizer != null)
		{
            EventStat ThisEvent = EventMemorizer.GetComponent<EventStat>();

            conditionMet = (Mathf.Clamp(ThisEvent.strReq - strHave, 0, 50) + Mathf.Clamp(ThisEvent.agiReq - agiHave, 0, 50) + Mathf.Clamp(ThisEvent.intReq - intHave, 0, 50) + 1 );

            if (playerAttack >= int.Parse(ThisEvent.AttackText.text) * 3)
            {
                conditionMet -= 2;
            }
            else if (playerAttack >= int.Parse(ThisEvent.AttackText.text))
            {
                conditionMet -= 1;
            }

            conditionMet  = Mathf.Clamp(conditionMet,0,4);

            switch (conditionMet)
			{
                case 4:
                    ThisEvent.Chance = 5;
                    succesChance.text = "This is a BAD IDEA!";
                    break;
                case 3:
                    ThisEvent.Chance = 30;
                    succesChance.text = "There is a bit of chance...";
                    break;
                case 2:
                    ThisEvent.Chance = 50;
                    succesChance.text = "I think it 50:50 here";
                    break;
                case 1:
                    ThisEvent.Chance = 75;
                    succesChance.text = "This one is easy!";
                    break;
                case 0:
                    ThisEvent.Chance = 105;
                    succesChance.text = "It's a sure win!";
                    break;
            }
        }
    }

    private float DayMultiCal()
    {
        float dayMulti = 1;

        if (timeManager.DayCount < 6)
        {
            dayMulti = 1;
        }
        else if (timeManager.DayCount < 12)
        {
            dayMulti = 1.2f;
        }
        else if (timeManager.DayCount < 20)
        {
            dayMulti = 1.4f;
        }
        return dayMulti;
    }

    private float TowerMultiCal()
    {
        float towerMulti = 1;
        switch (upgradePanel.TowerButtonIndex)
        {
            case 0:
                towerMulti = 1;
                break;
            case 1:
                towerMulti = 1;
                break;
            case 2:
                towerMulti = 1.2f;
                break;
            case 3:
                towerMulti = 1.4f;
                break;
            case 4:
                towerMulti = 1.6f;
                break;
            case 5:
                towerMulti = 1.6f;
                break;
        }
        return towerMulti;
    }

    public void checkRefresh()
	{
        if (resourceCalculator.Money < 400)
        {
            RefreshQuestButton.interactable = false;
        }
        else if (tutorial == null)
        {
            RefreshQuestButton.interactable = true;
        }
        else if (tutorial.tutorialFinish == true)
        {
            RefreshQuestButton.interactable = true;
        }
    }
    // Cancel personnel choosing tab
    public void cancleTab(GameObject tab)
	{
        foreach (Transform child in AvaiablePersonnelSorter.transform)
        {
			Destroy(child.transform.gameObject);
		}

        for(var i = 0; i < EventAvailableSorter.transform.childCount; i++)
		{
            if (EventAvailableSorter.GetChild(i).GetComponent<EventStat>().Mark == true)
			{
                EventAvailableSorter.GetChild(i).GetComponent<EventStat>().Mark = false;
            }
		}
        tab.SetActive(false);
    }
    // mark personnel for embark
    public void MarkPersonnel()
	{
       PersonnelStat thisPersonnel = EventSystem.current.currentSelectedGameObject.GetComponent<PersonnelStat>();
       thisPersonnel.Mark = !thisPersonnel.Mark;

        int index = Random.Range(0, 4);

        if (index == 1)
        {
            //AudioManager.instance.Play("FireSound" + Random.Range(1, 6).ToString());
        }
        else
        {
            AudioManager.instance.Play("Button");
        }
    }

    public void RefreshQuest()
    {
        resourceCalculator.MoneyPaid(400);

        addEvent();
    }

}
