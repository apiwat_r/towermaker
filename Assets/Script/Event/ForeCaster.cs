using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ForeCaster : MonoBehaviour
{
    public enum forecastStat { neutral, str, intel, agi, str_int, int_agi, agi_str };
    public forecastStat forecastATR;
    [SerializeField] private Image forecastImage;
    [SerializeField] private List<Sprite> attirbuteImage = new List<Sprite>();
    [SerializeField] private TextMeshProUGUI forecastText;
    [SerializeField] private TextMeshProUGUI forecastInfo;
    [SerializeField] private Animator animator;
    [SerializeField] private List<string> headerBank = new List<string>();
    [SerializeField] private List<string> textBank = new List<string>();
    private int statForecast = 0;

	private void Start()
	{
        animator.Play("ForeCastIn");
	}

	public void ForeCastStat()
	{
        statForecast++;

        if (statForecast == 5)
        { 
            int forecastRandomizer = Random.Range(0, 57);

            forecastText.color = Color.yellow;

            if (forecastRandomizer < 14)
            {
                forecastATR = forecastStat.neutral;
                forecastImage.sprite = attirbuteImage[3];
                forecastText.text = "Quest stat are balance today!";
                forecastInfo.text = "Quest stat are balance today!";
            }
            else if (forecastRandomizer < 21)
            {
                forecastATR = forecastStat.str;
                forecastImage.sprite = attirbuteImage[0];
                forecastText.text = "There a large surge in STR quest!";
                forecastInfo.text = "There a large surge in STR quest!";
            }
            else if (forecastRandomizer < 28)
            {
                forecastATR = forecastStat.agi;
                forecastImage.sprite = attirbuteImage[1];
                forecastText.text = "There a large surge in AGI quest!";
                forecastInfo.text = "There a large surge in AGI quest!";
            }
            else if (forecastRandomizer < 35)
            {
                forecastATR = forecastStat.intel;
                forecastImage.sprite = attirbuteImage[2];
                forecastText.text = "There a large surge in INT quest!";
                forecastInfo.text = "There a large surge in INT quest!";
            }
            else if (forecastRandomizer < 42)
            {
                forecastATR = forecastStat.str_int;
                forecastImage.sprite = attirbuteImage[4];
                forecastText.text = "There a large surge in STR/INT quest!";
                forecastInfo.text = "There a large surge in STR/INT quest!";
            }
            else if (forecastRandomizer < 49)
            {
                forecastATR = forecastStat.int_agi;
                forecastImage.sprite = attirbuteImage[5];
                forecastText.text = "There a large surge in INT/AGI quest!";
                forecastInfo.text = "There a large surge in INT/AGI quest!";
            }
            else if (forecastRandomizer < 56)
            {
                forecastATR = forecastStat.agi_str;
                forecastImage.sprite = attirbuteImage[6];
                forecastText.text = "There a large surge in AGI/STR quest!";
                forecastInfo.text = "There a large surge in AGI/STR quest!";
            }

            statForecast = -1;
        }
		else
		{
            forecastText.color = Color.white;
            forecastText.text = headerBank[Random.Range(0, headerBank.Count)]+ " " + textBank[Random.Range(0,textBank.Count)];
        }
    }

    public void ForecastShift()
    {
        animator.Play("ForeCastOut");
    }
}
