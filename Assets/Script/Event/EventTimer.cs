using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using TMPro;

public class EventTimer : MonoBehaviour
{
     private TimeManager timeManager;
    [SerializeField] private EventStat eventStat;
    [SerializeField] private Image FillGauge;
	[SerializeField] private TextMeshProUGUI timeText;
	 private ResourceCalculator resourceCalculator;
	[SerializeField] private GameObject QuestCompleteScreen;
	[SerializeField] private GameObject QuestFailScreen;
	[SerializeField] private GameObject Dummy;
	private Tutorial tutorial;
	public List<GameObject> PersonnelMemo = new List<GameObject>();

	private float TimeLeft =  0;
	private float TimeShow;
	private float InitialTime;
	private float randomNumber;

	private bool finish = false;

	private bool questFail;

	// Update is called once per frame

	private void Start()
	{
		tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();
		resourceCalculator = GameObject.FindGameObjectWithTag("GameController").GetComponent<ResourceCalculator>();
		timeManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<TimeManager>();
		InitialTime = eventStat.TimeConsume;
		TimeShow = InitialTime;
	}
	void Update()
    {
		questProgress();
		timeText.text = "Time Left: "+ TimeShow.ToString("0");
	}

	void questProgress()
	{

		TimeLeft += Time.deltaTime * timeManager.TimeScale;
		TimeShow -= Time.deltaTime * timeManager.TimeScale;
		TimeShow = Mathf.Clamp(TimeShow, 0, 500);

		FillGauge.fillAmount = (TimeLeft/InitialTime);


		if (FillGauge.fillAmount == 1 && !finish)
		{
			randomNumber = Random.Range(0, 100);

			if(randomNumber <= eventStat.Chance)
			{
				QuestCompleteScreen.SetActive(true);
				questFail = false;
			}
			else 
			{
				QuestFailScreen.SetActive(true);
				questFail = true;
			}

			if (tutorial != null )
			{
				if (tutorial.tutorialIndex == 9)
				{
					timeManager.timeScale(0);

					tutorial.DisableTimeButton();
				}
				
			}

			finish = true;
		}
	}
	public void AddPersonnel(GameObject personnel)
	{
		PersonnelMemo.Add(personnel);
	}

	public void QuestConfirm()
	{
		for (var i = 0; i <PersonnelMemo.Count; i++)
		{
			PersonnelMemo[i].GetComponent<PersonnelStat>().onQuest = false;
		}

		if (questFail)
		{
			Destroy(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);
		}
		else
		{
			resourceCalculator.GetResource(eventStat.MoneyStat);
			Destroy(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);
		}

		if (tutorial != null)
		{
			tutorial.continueTutorial();
		}

		AudioManager.instance.Play("Kaching");		
	}
}
