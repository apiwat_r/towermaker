using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EventStat : MonoBehaviour
{
    [SerializeField] private GameObject AttributeSoter;
    [SerializeField] private GameObject DummySorter;
    public Image image;

    public TextMeshProUGUI NameText;
    public TextMeshProUGUI MoneyText;
    public TextMeshProUGUI AttackText;
    public TextMeshProUGUI TimeText;

     public int AttackStat;
     public int MoneyStat;
     public float TimeConsume;
     public int Chance;

    private TimeManager timeManager;
    private ForeCaster foreCaster;
    public int RequireAttribute = 0;
    private int AttributeFill = 0;
    public int strReq { get; private set; } = 0;
    public int agiReq { get; private set; } = 0;
    public int intReq { get; private set; } = 0;

    public bool Mark = false;

    private int strWeight = 0;
    private int agiWeight = 0;
    private int intWeight = 0;

    [SerializeField] private List<GameObject> AttributeImage = new List<GameObject>();
    [SerializeField] private List<int> weightList = new List<int>();
    [SerializeField] private bool setUp = true;

    private Tutorial tutorial;
    public bool tutorialCheck = false;

	private void Start()
    {
        tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();
        timeManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<TimeManager>();
        foreCaster = GameObject.FindGameObjectWithTag("Forecaster").GetComponent<ForeCaster>();

        if (setUp)
		{
            QuestSetup();
        }
        else if (tutorial != null && tutorialCheck)
		{
            strReq = 1;
            Instantiate(AttributeImage[0], AttributeSoter.transform);
            for (var i = 0; i < 2; i++)
            {
                AttributeFill++;
                Instantiate(AttributeImage[3], AttributeSoter.transform);
            }
        }

     
    }

    private void QuestSetup()
	{
        forecastStat();

        for (var i = 0; i < RequireAttribute; i++)
        {
            int statMarker = calculateDrop();

                switch (statMarker)
                {
                    case 0:
                        strReq += 1;
                        break;
                    case 1:
                        agiReq += 1;
                        break;
                    case 2:
                        intReq += 1;
                        break;
                }

            AttributeFill++;

            Instantiate(AttributeImage[statMarker], AttributeSoter.transform);
        }

        for (var i = AttributeFill; i < 3; i++)
		{
            Instantiate(AttributeImage[3], AttributeSoter.transform);
		}
    }

    private void forecastStat()
	{
        if(foreCaster.forecastATR == ForeCaster.forecastStat.neutral)
		{
            strWeight = 33;
            agiWeight = 66;
        }
        else if (foreCaster.forecastATR == ForeCaster.forecastStat.str)
        {
            strWeight = 60;
            agiWeight = 80;
        }
        else if (foreCaster.forecastATR == ForeCaster.forecastStat.agi)
        {
            strWeight = 20;
            agiWeight = 80;
        }
        else if (foreCaster.forecastATR == ForeCaster.forecastStat.intel)
        {
            strWeight = 20;
            agiWeight = 40;
        }
        else if (foreCaster.forecastATR == ForeCaster.forecastStat.str_int)
        {
            strWeight = 40;
            agiWeight = 60;
        }
        else if (foreCaster.forecastATR == ForeCaster.forecastStat.int_agi)
        {
            strWeight = 20;
            agiWeight = 60;
        }
        else if (foreCaster.forecastATR == ForeCaster.forecastStat.agi_str)
        {
            strWeight = 40;
            agiWeight = 80;
        }
        weightList[0] = strWeight;
        weightList[1] = agiWeight;
        weightList[2] = intWeight;
    }

    public int calculateDrop()
	{
        int mark = 0;

        int randomNumber = Random.Range(0, 101);

        if(randomNumber <= weightList[0])
		{
            mark = 0;
		}
        else if (randomNumber <= weightList[1])
		{
            mark = 1;
        }
        else 
		{
            mark = 2;
        }

        return mark;
    }
}
