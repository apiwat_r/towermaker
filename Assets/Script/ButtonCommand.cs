using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonCommand : MonoBehaviour
{
	[SerializeField] private Transform menuList;
	[SerializeField] private Transform tipList;
	[SerializeField] private GameObject PersonnelChoosenTab;
	[SerializeField] private GameObject AvailablePersonnelTab;
	[SerializeField] private GameObject PauseScreen;
	private Tutorial tutorial;

	private void Start()
	{
		tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();	
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) && PauseScreen != null)
		{
			PauseScreen.SetActive(!PauseScreen.activeSelf);

			if (PauseScreen.activeSelf == false)
			{
				Time.timeScale = 1;
			}
			else
			{
				Time.timeScale = 0;
			}
		}
	}

	public void MenuSelect(GameObject menu)
	{
		foreach (Transform child in menuList)
		{
			child.gameObject.SetActive(false);
		}

		menu.SetActive(true);

		foreach (Transform child in AvailablePersonnelTab.transform)
		{
			Destroy(child.gameObject);
		}
		PersonnelChoosenTab.SetActive(false);

		foreach (Transform child in tipList)
		{
			child.gameObject.SetActive(false);
		}

		AudioManager.instance.Play("Button");

		if(tutorial != null)
		{
			if(tutorial.tutorialIndex != 6 && tutorial.tutorialIndex != 9 && tutorial.tutorialIndex != 10)
			{
				tutorial.continueTutorial();
			}
			
			if(tutorial.tutorialIndex == 6)
			{
				tutorial.PanelMask.SetActive(true);
			}
		}

	}

	public void LoadScene(string sceneName)
	{
		Transition.instance.Trans(sceneName);

		AudioManager.instance.Play("Button");

		Time.timeScale = 1;
	}

	public void resumeGame(GameObject gameObject)
	{
		gameObject.SetActive(!gameObject.activeSelf);

		if (gameObject.activeSelf == false)
		{
			Time.timeScale = 1;
		}
		else
		{
			Time.timeScale = 0;
		}

		AudioManager.instance.Play("Button");
	}

	public void showTip(GameObject gameObject)
	{
		foreach (Transform child in tipList)
		{
			child.gameObject.SetActive(false);
		}

		gameObject.SetActive(true);
	}

	public void closeTip()
	{
		gameObject.SetActive(false);
	}



}
