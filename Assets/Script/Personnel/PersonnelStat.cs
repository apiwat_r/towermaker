using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Linq;

public class PersonnelStat : MonoBehaviour
{
	public enum Attribute {strength,speed,intelligent}
	public Attribute attributes;
	public Attribute secondaryAttributes;
	public Image image;
	public TextMeshProUGUI NameText;
	public  TextMeshProUGUI MoneyText;
	public TextMeshProUGUI AttackText;
	public TextMeshProUGUI Special;
	public TextMeshProUGUI Status;

	public GameObject PersonnelMemorizer;
	[HideInInspector] public int AttackStat;
	[HideInInspector] public int FoodStat;
	[HideInInspector] public int MoneyStat;
	[HideInInspector] public float MoneyRequire;
	[HideInInspector] public bool onQuest = false;
	 public bool Mark = false;

	public Image AttributeType;
	[SerializeField] private List<Sprite> AttributeImage = new List<Sprite>();
	[SerializeField] private bool randomizeStat =true;

	private void Start()
	{
		if (randomizeStat)
		{
			attributes = (Attribute)Random.Range(0, System.Enum.GetValues(typeof(Attribute)).Length);
		}
	
		switch (attributes)
		{
			case Attribute.strength:
				AttributeType.sprite = AttributeImage[0];
				break;

			case Attribute.speed:
				AttributeType.sprite = AttributeImage[1];
				break;

			case Attribute.intelligent:
				AttributeType.sprite = AttributeImage[2];
				break;
		}
	}

	private void Update()
	{
		if (Status == null)
		{
			return;
		}

		if(onQuest == true)
		{
			Status.text = "Status: On Quest";
		}
		else
		{
			Status.text = "Status: Available";
		}
	}

	public void Tutorial()
	{
		AttributeType.sprite = AttributeImage[0];
	}

}
