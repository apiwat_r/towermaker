using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.EventSystems;
public class ResourceCalculator : MonoBehaviour
{
    [SerializeField] private UpgradePanel upgradePanel;
    [SerializeField] private TextMeshProUGUI PersonnelText;
    [SerializeField] private TextMeshProUGUI MoneyText;
    [SerializeField] private TextMeshProUGUI AttactText;
    [SerializeField] private GameObject PersonnelUI;
    [SerializeField] private GameObject PersonnelActiveUI;
    [SerializeField] private Transform PersonnelRecruitmentSorter;
    [SerializeField] private Transform PersonnelActiveSorter;
    [SerializeField] private TimeManager timeManager;
    [SerializeField] private List<PersonnelTemplate> PersonnelPool = new List<PersonnelTemplate>();
    [SerializeField] private GameObject tutorialRecruit;
    private Tutorial tutorial;

    [SerializeField] private Button RefreshPersonnelButton;

    public float Money { get; private set; } = 500;
    public float Attack { get; private set; } = 0;

    public float personnelCap { get; private set; } = 4;
    public float personnelAmount { get; private set;} = 0;

    private int eventIndexPersonnel = 0;

    private bool tutorialCheck = true;
    private bool assit = true;
    private float assitTimer = 1f;

	private void Start()
	{
        tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();
        addPersonnel();
    }

	void Update()
    {
        if (Input.GetKeyDown(KeyCode.K))
        {
            Money = 99999;
            
        }

        PersonnelText.text = personnelAmount.ToString() + "/" + personnelCap.ToString();
        Calculate_Resource();
        MoneyText.text = Money.ToString("0");

        if (assit == false)
		{
            assitTimer -= Time.deltaTime * timeManager.TimeScale;
        }

		if (assitTimer <= 0)
		{
            assitTimer = 1;

            assit = true;
		}

        if (Money < 250)
		{
            RefreshPersonnelButton.interactable = false;
		}
        else if (tutorial == null)
		{
            RefreshPersonnelButton.interactable = true;
        }
        else if (tutorial.tutorialFinish == true)
		{
            RefreshPersonnelButton.interactable = true;
        }
    }

    public void Calculate_Resource()
    {
        Attack = 0;
        for (var i = 0; i < PersonnelActiveSorter.transform.childCount; i++)
        {
            PersonnelStat thisPersonnel = PersonnelActiveSorter.transform.GetChild(i).GetComponent<PersonnelStat>();

            if (thisPersonnel.onQuest == false)
			{
                Attack += int.Parse(thisPersonnel.AttackText.text);
            }
            
        }
        AttactText.text = Attack.ToString("0");     
    }    
      
    public void addPersonnel()
    {
        for (var i = 0; i < PersonnelRecruitmentSorter.transform.childCount; i++)
		{
            Destroy(PersonnelRecruitmentSorter.transform.GetChild(i).gameObject);
		}

        for(var i = 0; i <5; i++)
		{
            eventIndexPersonnel = Random.Range(0, PersonnelPool.Count);

            float MoneyRequire =  Mathf.Ceil(PersonnelPool[eventIndexPersonnel].MoneyRequire * Random.Range(0.7f, 1.5f)  * DayMultiCal());

            PersonnelUI.GetComponent<PersonnelStat>().NameText.text = PersonnelPool[eventIndexPersonnel].name;
            PersonnelUI.GetComponent<PersonnelStat>().Special.text = PersonnelPool[eventIndexPersonnel].speechBank[Random.Range( 0, PersonnelPool[eventIndexPersonnel].speechBank.Count)];
            PersonnelUI.GetComponent<PersonnelStat>().image.sprite = PersonnelPool[eventIndexPersonnel].artwork;
            PersonnelUI.GetComponent<PersonnelStat>().MoneyRequire = MoneyRequire;
            PersonnelUI.GetComponent<PersonnelStat>().MoneyText.text = MoneyRequire.ToString();
            PersonnelUI.GetComponent<PersonnelStat>().AttackText.text = Mathf.Ceil(PersonnelPool[eventIndexPersonnel].attackStat * Random.Range(0.7f, 1.5f) * DayMultiCal() * TowerMultiCal()).ToString();

            if (PersonnelActiveSorter.childCount == 0 && assit && tutorial == null)
            {
                PersonnelUI.GetComponent<PersonnelStat>().NameText.text = PersonnelPool[2].name;
                PersonnelUI.GetComponent<PersonnelStat>().Special.text = PersonnelPool[2].speechBank[Random.Range(0, PersonnelPool[2].speechBank.Count)];
                PersonnelUI.GetComponent<PersonnelStat>().image.sprite = PersonnelPool[2].artwork;
                PersonnelUI.GetComponent<PersonnelStat>().MoneyRequire = 0;
                PersonnelUI.GetComponent<PersonnelStat>().MoneyText.text = "0";
                PersonnelUI.GetComponent<PersonnelStat>().AttackText.text = Mathf.Ceil(PersonnelPool[2].attackStat * Random.Range(0.7f, 1.5f) * DayMultiCal() * TowerMultiCal()).ToString();

                assit = false;
            }

            if (tutorial != null &&  tutorialCheck == true)
            {
                Instantiate(tutorialRecruit, PersonnelRecruitmentSorter);

                tutorialCheck = false;
            }
			else
			{
                Instantiate(PersonnelUI, PersonnelRecruitmentSorter);
            }
        }

    }
    public void hirePersonnel()
	{
        personnelAmount++;
        PersonnelStat ChoosenPersonnel = EventSystem.current.currentSelectedGameObject.GetComponent<PersonnelStat>();

        MoneyPaid(ChoosenPersonnel.MoneyRequire);
        PersonnelActiveUI.GetComponent<PersonnelStat>().NameText.text = ChoosenPersonnel.NameText.text;
        PersonnelActiveUI.GetComponent<PersonnelStat>().AttackText.text = ChoosenPersonnel.AttackText.text;
        PersonnelActiveUI.GetComponent<PersonnelStat>().Special.text = ChoosenPersonnel.Special.text;
        PersonnelActiveUI.GetComponent<PersonnelStat>().image.sprite = ChoosenPersonnel.image.sprite;
        PersonnelActiveUI.GetComponent<PersonnelStat>().attributes = ChoosenPersonnel.attributes;
  
        Instantiate(PersonnelActiveUI, PersonnelActiveSorter);
        Destroy(EventSystem.current.currentSelectedGameObject);

        int index = Random.Range(0, 4);

        if(index == 1)
		{
            AudioManager.instance.Play("HireSound" + Random.Range(1, 12).ToString());
		}
		else
		{
            AudioManager.instance.Play("Button");
        }


        if(tutorial != null)
		{
            tutorial.continueTutorial();
		}
}

	public void removepersonnel()
	{
		personnelAmount--;
		Destroy(EventSystem.current.currentSelectedGameObject.transform.parent.gameObject);

        int index = Random.Range(0, 4);

        if (index == 1)
        {
            AudioManager.instance.Play("FireSound" + Random.Range(1, 6).ToString());
        }
        else
        {
            AudioManager.instance.Play("Button");
        }
    }

    private float DayMultiCal()
	{
        float dayMulti = 1;

        if(timeManager.DayCount < 6)
		{
            dayMulti = 1;
		}
        else if (timeManager.DayCount < 12)
		{
            dayMulti = 1.2f;
        }
        else if (timeManager.DayCount < 20)
        {
            dayMulti = 1.3f;
        }
        return dayMulti;
    }

    private float TowerMultiCal()
	{
        float towerMulti = 1;
		switch (upgradePanel.TowerButtonIndex)
		{
            case 0:
                towerMulti = 1;
                break;
            case 1:
                towerMulti = 1;
                break;
            case 2:
                towerMulti = 1.2f;
                break;
            case 3:
                towerMulti = 1.2f;
                break;
            case 4:
                towerMulti = 1.4f;
                break;
            case 5:
                towerMulti = 1.4f;
                break;

        }
        return towerMulti;
	}

    public  void RefreshPersonnel()
	{
        Money = Mathf.Clamp(Money, 0, 500000000000000000);
        MoneyPaid(250);

        addPersonnel();
	}

    public void MoneyPaid(float moneyPaid)
	{
        Money -= moneyPaid;
	}

    public void GetResource(float moneyGet)
	{
        Money += moneyGet;
	}

    public void IncreasePersonnel()
	{
        personnelCap++;
	}
}