using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundAdjust : MonoBehaviour
{
	[SerializeField] private AudioMixer Mixer;

	public bool musicToggle { get; private set; } = true;
	public bool sfxToggle { get; private set; } = true;

	public void setMusic()
	{
		musicToggle = !musicToggle;

		if(musicToggle == false)
		{
			Mixer.SetFloat("musicVolume",-80);
		}
		else
		{
			Mixer.SetFloat("musicVolume", 0);
		}

		AudioManager.instance.Play("Button");
	}

	public void setSFX()
	{
		sfxToggle = !sfxToggle;

		if (sfxToggle == false)
		{
			Mixer.SetFloat("sfxVolume", -80);
		}
		else
		{
			Mixer.SetFloat("sfxVolume", 0);
			AudioManager.instance.Play("Button");
		}
		
	}
}
