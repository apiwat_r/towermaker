using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloudSpawner : MonoBehaviour
{
    [SerializeField] private GameObject cloud;
    [SerializeField] private float MaxY = 0;
    [SerializeField] private float MinY = 0;
    [SerializeField] private float CloudTimer = 3;
    private float chooseY;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        CloudTimer -= Time.deltaTime;

        if(CloudTimer <= 0)
		{
            chooseY = Random.Range(MinY, MaxY);
            Instantiate(cloud, new Vector3(transform.position.x, chooseY, 0), Quaternion.identity);

            CloudTimer = 3;
		}
    }
}
