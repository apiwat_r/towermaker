using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Cloud : MonoBehaviour
{

    [SerializeField] private List<Sprite> CloudList = new List<Sprite>();
    [SerializeField] private SpriteRenderer CloudImage;
    private float moveSpeed = 2;
    private float timer = 10;
    // Start is called before the first frame update
    void Start()
    {
        CloudImage.sprite = CloudList[Random.Range(0, 3)];
        moveSpeed = Random.Range(2, 4);
    }

    // Update is called once per frame
    void Update()
    {
        timer -= Time.deltaTime;

        transform.Translate(new Vector3(1, 0) * moveSpeed * Time.deltaTime);

  //      if(timer <= 0)
		//{
  //          Destroy(gameObject);
		//}
    }

	private void OnCollisionEnter2D(Collision2D collision)
	{
        if (collision.gameObject.tag == "CouldDestroyer")
		{
            Destroy(gameObject);
        }
     
	}
}
