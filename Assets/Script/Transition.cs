using UnityEngine.Audio;
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Transition : MonoBehaviour
{

	public static Transition instance;
	public Animator animator;
	public string sceneName;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);

	}

	public void Trans(string scene)
	{
		sceneName = scene;
		animator.Play("Fade");
	}

	public void sceneChoose() 
	{
		SceneManager.LoadScene(sceneName);
	}

}
