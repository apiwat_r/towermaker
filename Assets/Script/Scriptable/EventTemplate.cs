using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[CreateAssetMenu(fileName = "New Event", menuName = "Event")]
public class EventTemplate : ScriptableObject
{
    public int tier;

    public new string name;
    public string description;
    public string special;

    public Sprite artwork;

    public int reward;

    public int timeConsume;
    public int requireAttack;

    public enum ArtifactReward { no, gurantee, chance, randomGurantee }
    public ArtifactReward artifactReward;
    public int artifactChance;

}
