using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Personnel", menuName = "Personnel")]
public class PersonnelTemplate : ScriptableObject
{
    public int tier;

    public new string name;
    public string description;
    public string special;

    public Sprite artwork;

    public int MoneyRequire;
    public int attackStat;

    public List<string> speechBank = new List<string>();
}
