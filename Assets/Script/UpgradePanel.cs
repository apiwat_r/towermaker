using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

public class UpgradePanel : MonoBehaviour
{
    [SerializeField] private ResourceCalculator resourceCalculator;
    [SerializeField] private TimeManager timeManager;
    private Tutorial tutorial;
    [SerializeField] private Button TowerButton;
    [SerializeField] private List<int> TowerCost = new List<int>();
    [SerializeField] private List<GameObject> Tower = new List<GameObject>();
    [SerializeField] private List<GameObject> TowerUI = new List<GameObject>();
    [SerializeField] private TextMeshProUGUI UpgradeText;
    [SerializeField] private TextMeshProUGUI UpgradeCost;
    [SerializeField] private GameObject GameWin;
    public int TowerButtonIndex { get; private set; } = 0;


	private void Start()
	{
        tutorial = GameObject.FindGameObjectWithTag("GameController").GetComponent<Tutorial>();
    }

	private void Update()
	{
            if (TowerCost[TowerButtonIndex] > resourceCalculator.Money)
            {
                TowerButton.interactable = false;
            }
            else
            {
                TowerButton.interactable = true;
            }
	}

    public void UpgradeTower()
    {
        AudioManager.instance.Play("Button");

        if(tutorial != null && tutorial.tutorialIndex == 10)
		{
            tutorial.continueTutorial();
		}

        TowerButtonIndex += 1;

 
        Tower[TowerButtonIndex].SetActive(true);
        TowerUI[TowerButtonIndex].SetActive(true);
        


        if(TowerButtonIndex != 5)
		{
            resourceCalculator.MoneyPaid(TowerCost[TowerButtonIndex -1]);
            UpgradeCost.text = "Tier" + (TowerButtonIndex + 1).ToString("0") + ": $" + TowerCost[TowerButtonIndex].ToString("0");
        }

        resourceCalculator.IncreasePersonnel();

        switch (TowerButtonIndex)
		{
            case 1:
                UpgradeText.text = "Increase personnel Limit to 6";
                break;
          
            case 2:
                UpgradeText.text = "Increase personnel Limit to 7";
                break;

            case 3:
                UpgradeText.text = "Increase personnel Limit to 8";
                break;

            case 4:
                UpgradeText.text = "Win the game!";
                break;

            case 5:
                    GameWin.SetActive(true);
                AudioManager.instance.Play("Winning");
                    timeManager.timeScale(0);
                    break;
        }
    }
}
