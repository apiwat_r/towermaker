using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIPrefab : MonoBehaviour
{
	private ResourceCalculator resourceCalculator;
	private EventManager eventManager;
	private GameObject gameController;
	[SerializeField] private Image ButtonPic;
	[SerializeField] private PersonnelStat personnelStat;
	[SerializeField] private Button HireButton;
	[SerializeField] private Button FireButton;
	[SerializeField] private Button ChooseQuest;
	[SerializeField] private Button MarkButton;
	[SerializeField] private Button QuestionButton;
	[SerializeField] private Button musicButton;
	[SerializeField] private Button sfxButton;


	private void Start()
	{
		gameController = GameObject.FindGameObjectWithTag("GameController");
		resourceCalculator = gameController.GetComponent<ResourceCalculator>();
		eventManager = gameController.GetComponent<EventManager>();

		if (MarkButton != null)
		{
			MarkButton.onClick.AddListener(eventManager.MarkPersonnel);
		}

		if (ChooseQuest != null)
		{
			ChooseQuest.onClick.AddListener(eventManager.activateEvent);
		}

		if (HireButton != null)
		{
			HireButton.onClick.AddListener(resourceCalculator.hirePersonnel);
		}
		
		if(FireButton != null)
		{
			FireButton.onClick.AddListener(resourceCalculator.removepersonnel);
		}
	}
	private void Update()
	{
		HireAble();
		checkMark();
		checkAvaiable();
		
	}

	void HireAble()
	{
		if(HireButton == null)
		{
			return;
		}
		else
		{
			if (resourceCalculator.Money < personnelStat.MoneyRequire || resourceCalculator.personnelAmount >= resourceCalculator.personnelCap)
			{
				HireButton.interactable = false;
			}
			else
			{
				HireButton.interactable = true;
			}
		}

	}

	private void checkAvaiable()
	{
		if(FireButton == null)
		{
			return;
		}

		if (personnelStat.onQuest)
		{
			FireButton.interactable = false;
		}
		else
		{
			FireButton.interactable = true;
		}
	}

	void checkMark()
	{
		if (MarkButton == null)
		{
			return;
		}
		else
		{
			if(personnelStat.Mark == false)
			{
				ButtonPic.gameObject.SetActive(true);
			}
			else if (personnelStat.Mark == true)
			{
				ButtonPic.gameObject.SetActive(false);
			}
		}
	}
}
