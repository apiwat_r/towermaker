using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI tutorialText;
    [SerializeField] private GameObject QuestSorter;
    [SerializeField] private GameObject PersonnelSorter;
    [SerializeField] private Button ContinueButton;
    [SerializeField] private GameObject Mask1;
    [SerializeField] private GameObject Mask2;
    [SerializeField] private GameObject TabMask;
    public GameObject PanelMask;
    [SerializeField] private Animator TutorialAnimatior;
    [SerializeField] private Animator TabAnimator;
    [SerializeField] private GameObject TutorialTab;
    [SerializeField] private GameObject AvialablePersonnelSorter;
    [SerializeField] private Button embarkButton;
    [SerializeField] private Button cancleButton;
    [SerializeField] private List<Button> InstructButtonList = new List<Button>();
    [SerializeField] private List<Button> TimeButton = new List<Button>();
    [SerializeField] private List<Button> TabButton = new List<Button>();
    [SerializeField] private GameObject tutorialWinScreen;

    private ResourceCalculator resourceCalculator;
    private TimeManager timeManager;
    private UpgradePanel upgradePanel;
    public int tutorialIndex { get; private set; } = 0;

    public bool tutorialFinish { get; private set; } = false;
    private bool win = false;
    
    // Start is called before the first frame update
    void Start()
    {
        for (var i =  0; i < InstructButtonList.Count; i++)
		{
            InstructButtonList[i].interactable = false;
		}

        for (var i = 0; i < TimeButton.Count; i++)
        {
            TimeButton[i].interactable = false;
        }

		timeManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<TimeManager>();
        resourceCalculator = GameObject.FindGameObjectWithTag("GameController").GetComponent<ResourceCalculator>();
        upgradePanel = GameObject.FindGameObjectWithTag("GameController").GetComponent<UpgradePanel>();

        resourceCalculator.MoneyPaid(500);
        timeManager.timeScale(0);
    }

	private void Update()
	{
	    if(upgradePanel.TowerButtonIndex == 2 && !win)
		{
            timeManager.timeScale(0);
            tutorialWinScreen.SetActive(true);
            AudioManager.instance.Play("Winning");
            win = true;
    

        }
	}


    public void continueTutorialButton()
	{
        continueTutorial();

        AudioManager.instance.Play("Button");
	}

	public void continueTutorial()
    {
        tutorialIndex++;
        switch (tutorialIndex)
        {
            case 1:
                Mask1.SetActive(true);
                tutorialText.text = "In this game, your goal is to upgrade your tower from tier 1 to 5. \nBefore the end of 20th day.";
                break;
            case 2:
                tutorialText.text = "To do that you need money, So let's get some!";
                break;
            case 3:
                tutorialText.text = "To get money you need to do quest! And to do quest you need some personnel!";
                break;
            case 4:
                Mask1.SetActive(false);
                TabMask.SetActive(true);
                EnableButton(1);
                TabAnimator.Play("ToPersonnel");
                tutorialText.text = "Press the personel tab to hire some personnel.";
                ContinueButton.interactable = false;
                break;
            case 5:
                TabMask.SetActive(false);
                PanelMask.SetActive(true);
                EnableButton(3);
                tutorialText.text = "Press the blue panel to hire a personnel. For now, hire bobby";
                break;
            case 6:
                EnableButton(2);
                TabMask.SetActive(true);
                PanelMask.SetActive(false);
                TabAnimator.Play("ToQuest");
                tutorialText.text = "Let's send your personnnel to quest! Press the quest tab and choose herb gathering!";
                break;
            case 7:
                QuestSorter.transform.GetChild(0).GetComponent<Button>().interactable = false;
                PanelMask.SetActive(false);
                TabMask.SetActive(false);
                Mask1.SetActive(true);
                embarkButton.interactable = false;
                cancleButton.interactable = false;
                EnableButton(3);
                
                for(var i = 0; i < AvialablePersonnelSorter.transform.childCount; i++)
				{
                    AvialablePersonnelSorter.transform.GetChild(i).GetComponent<Button>().interactable = false;
				}

                tutorialText.text = "In assignment tab, you need to choose your personnel and send them on a quest!";
                ContinueButton.interactable = true;
                break;
            case 8:
                for (var i = 0; i < AvialablePersonnelSorter.transform.childCount; i++)
                {
                    AvialablePersonnelSorter.transform.GetChild(i).GetComponent<Button>().interactable = true;
                }
                ContinueButton.interactable = false;
                embarkButton.interactable = true;
                tutorialText.text = "You have a personnel with enough attack and strength attribute. Choose them and then press the embark button!";
                break;

            case 9:
                TimeButton[1].interactable = true;
                TabMask.SetActive(true);
                TabAnimator.Play("ToTime");
                tutorialText.text = "Now change the time speed to x1 so the quest can progress! Don't forget to click the quest when it's done!";
                break;

            case 10:
                EnableButton(0);
                TabMask.SetActive(true);
                TabAnimator.Play("ToTower");
                timeManager.timeScale(0);
           
                for (var i = 0; i < TimeButton.Count; i++)
                {
                    TimeButton[i].interactable = false;
                }
                tutorialText.text = "Now that you have enough money to upgrade the tower! Go to the tower tab and upgrade it";
                break;
            case 11:
                TabMask.SetActive(false);
                tutorialText.text = "Good! now you can hire even more personnel!";
                ContinueButton.interactable = true;
                break;
            case 12:
                Mask1.SetActive(false);
                Mask2.SetActive(true);
                tutorialText.text = "Finally, check the forecast! It will tell you what sort of attribute a quest will have that day.";
                break;
            case 13:
                Mask2.SetActive(false);
                tutorialText.text = "And that's the basic! Hope you have fun in the game!";
                break;
            case 14:
                Transition.instance.Trans("Title");
                
                break;

        }
    }

    private void EnableButton(int x)
    {
        for(var i = 0; i < TabButton.Count; i++)
		{
            TabButton[i].interactable = false;
		}

        if(x != 3)
		{
            TabButton[x].interactable = true;
        }

    }

    public void DisableTimeButton()
	{
        TabMask.SetActive(false);

        for (var i = 0; i < TimeButton.Count; i++)
        {
            TimeButton[i].interactable = false;
        }
    }
}

